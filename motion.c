#include "motion.h"

/*************************************************************************************************/

void Motion_Solve_P2S3(double (*system)[3], double *dt, double *c)
{
	double dt0 = dt[0];
	double dt1 = dt[1];
	double dt2 = dt[2];
	double c0 = c[0];
	double c1 = c[1];
	double c2 = c[2];
	double c3 = c[3];
#include "motion_p2s3.txt"
}

void Motion_Solve_P3S7(double (*system)[4], double *dt, double *c)
{
	double dt0 = dt[0];
	double dt0_2 = dt0 * dt0;
	double dt0_3 = dt0_2 * dt0;
	double dt1 = dt[1];
	double dt1_2 = dt1 * dt1;
	double dt2 = dt[2];
	double dt2_2 = dt2 * dt2;
	double dt3 = dt[3];
	double dt4 = dt[4];
	double dt4_2 = dt4 * dt4;
	double dt5 = dt[5];
	double dt5_2 = dt5 * dt5;
	double dt6 = dt[6];
	double dt6_2 = dt6 * dt6;
	double dt6_3 = dt6_2 * dt6;
	double c0 = c[0];
	double c1 = c[1];
	double c2 = c[2];
	double c3 = c[3];
	double c4 = c[4];
	double c5 = c[5];
#include "motion_p3s7.txt"
}

/*************************************************************************************************/

void Motion_Solve_P4S3(double (*system)[5], double *dt, double *c)
{
	double dt0 = dt[0];
	double dt0_2 = dt0 * dt0;
	double dt0_3 = dt0_2 * dt0;
	double dt1 = dt[1];
	double dt2 = dt[2];
	double dt2_2 = dt2 * dt2;
	double dt2_3 = dt2_2 * dt2;
	double c0 = c[0];
	double c1 = c[1];
	double c2 = c[2];
	double c3 = c[3];
	double c4 = c[4];
	double c5 = c[5];
#include "motion_p4s3.txt"
}

void Motion_Solve_P6S3(double (*system)[7], double *dt, double *c)
{
	double dt0 = dt[0];
	double dt0_2 = dt0 * dt0;
	double dt0_3 = dt0_2 * dt0;
	double dt0_4 = dt0_3 * dt0;
	double dt0_5 = dt0_4 * dt0;
	double dt1 = dt[1];
	double dt2 = dt[2];
	double dt2_2 = dt2 * dt2;
	double dt2_3 = dt2_2 * dt2;
	double dt2_4 = dt2_3 * dt2;
	double dt2_5 = dt2_4 * dt2;
	double c0 = c[0];
	double c1 = c[1];
	double c2 = c[2];
	double c3 = c[3];
	double c4 = c[4];
	double c5 = c[5];
	double c6 = c[6];
	double c7 = c[7];
#include "motion_p6s3.txt"
}

/*************************************************************************************************/

void Motion_Solve_P1S1(double (*system)[2], double *dt, double *c)
{
	double dt0 = dt[0];
	double c0 = c[0];
	double c1 = c[1];
#include "motion_p1s1.txt"
}

void Motion_Solve_P3S1(double (*system)[4], double *dt, double *c)
{
	double dt0 = dt[0];
	double dt0_2 = dt0 * dt0;
	double dt0_3 = dt0_2 * dt0;
	double c0 = c[0];
	double c1 = c[1];
	double c2 = c[2];
	double c3 = c[3];
#include "motion_p3s1.txt"
}

void Motion_Solve_P5S1(double (*system)[6], double *dt, double *c)
{
	double dt0 = dt[0];
	double dt0_2 = dt0 * dt0;
	double dt0_3 = dt0_2 * dt0;
	double dt0_4 = dt0_3 * dt0;
	double dt0_5 = dt0_4 * dt0;
	double c0 = c[0];
	double c1 = c[1];
	double c2 = c[2];
	double c3 = c[3];
	double c4 = c[4];
	double c5 = c[5];
#include "motion_p5s1.txt"
}

void Motion_Solve_P7S1(double (*system)[8], double *dt, double *c)
{
	double dt0 = dt[0];
	double dt0_2 = dt0 * dt0;
	double dt0_3 = dt0_2 * dt0;
	double dt0_4 = dt0_3 * dt0;
	double dt0_5 = dt0_4 * dt0;
	double dt0_6 = dt0_5 * dt0;
	double dt0_7 = dt0_6 * dt0;
	double c0 = c[0];
	double c1 = c[1];
	double c2 = c[2];
	double c3 = c[3];
	double c4 = c[4];
	double c5 = c[5];
	double c6 = c[6];
	double c7 = c[7];
#include "motion_p7s1.txt"
}