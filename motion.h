#ifndef __MOTION_H
#define __MOTION_H

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/*************************************************************************************************/

void Motion_Solve_P2S3(double (*system)[3], double *dt, double *c);
void Motion_Solve_P3S7(double (*system)[4], double *dt, double *c);

/*************************************************************************************************/

void Motion_Solve_P4S3(double (*system)[5], double *dt, double *c);
void Motion_Solve_P6S3(double (*system)[7], double *dt, double *c);

/*************************************************************************************************/

void Motion_Solve_P1S1(double (*system)[2], double *dt, double *c);
void Motion_Solve_P3S1(double (*system)[4], double *dt, double *c);
void Motion_Solve_P5S1(double (*system)[6], double *dt, double *c);
void Motion_Solve_P7S1(double (*system)[8], double *dt, double *c);

/*************************************************************************************************/

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __MOTION_H */